import os, requests, threading, datetime, random
from pyvirtualdisplay import Display
from selenium import webdriver
from time import sleep

def send_start(worker_name):
    try:
        requests.post('http://anthill.duckdns.org:1784/cms/beacon/', data={'worker': worker_name}, timeout=5)
    except:
        print('err sent_beacon()')

def send_finish(worker_name, th, ah):
    try:
        requests.post('http://anthill.duckdns.org:1784/cms/', data={'worker': worker_name, 'th':th, 'ah':ah}, timeout=5)
    except:
        print('err send_finish()')

def main():
    with open('id', 'r') as f:
        worker_name = f.read()
    with open('node', 'r') as f:
        node = f.read()
    send_start(worker_name)
    display = Display(visible=0, size=(1366, 768))
    display.start()
    driver = webdriver.Firefox()
    driver.get(node)
    sleep(60*random.randint(5,20))
    th = driver.find_element_by_id('th').text
    ah = driver.find_element_by_id('ah').text
    driver.close()
    display.stop()
    send_finish(worker_name, th, ah)

if __name__ == '__main__':
    main()
#2018-02-26 15:03:01.309129
#2018-02-26 15:21:01.445591
#2018-02-26 15:44:01.407221
#2018-02-26 17:18:01.973992
#2018-02-26 18:03:01.187323
#2018-02-26 18:48:01.329140
#2018-02-26 19:32:01.823663
#2018-02-26 20:21:01.855866
#2018-02-26 21:20:01.507511
#2018-02-26 22:20:01.729346
#2018-02-26 23:31:01.252114
#2018-02-27 00:16:01.440131